//
//  ViewController.swift
//  WeatherForecast
//
//  Created by Michael Tugby on 15/10/2014.
//  Copyright (c) 2014 Michael Tugby. All rights reserved.
//

import UIKit
import CoreLocation

var savedLocations:NSUserDefaults = NSUserDefaults.standardUserDefaults()
var savedLatLon:NSUserDefaults = NSUserDefaults.standardUserDefaults()

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var locationView: UILabel!
    @IBOutlet var appsTableView : UITableView?
    
    let location = CLLocationManager()
    
    var datesView:[String] = []
    var weatherView:[String] = []
    var imagesView:[String] = []
    var latitude:Double?
    var longitude:Double?

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datesView.count
    }
    
    //Display the weather and date in the table.
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Weather", forIndexPath: indexPath) as WeatherCell
        
        cell.date!.text = datesView[indexPath.row]
        cell.weatherDesc!.text = weatherView[indexPath.row]
        cell.weatherImage!.image = UIImage(named: "\(imagesView[indexPath.row]).png")
        
        return cell
    }
    
    //On load, get users current location.
    override func viewDidLoad() {
        super.viewDidLoad()
        if savedLocations.valueForKey("LocationCity") == nil {
            CLLocationManager.locationServicesEnabled()
            self.location.requestWhenInUseAuthorization()
            self.location.delegate = self
            self.location.distanceFilter = 20
            self.location.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            self.location.startUpdatingLocation()
        }
        
        //If user has changed their location on the map view, display that instead.
        else {
            let oldLat = savedLatLon.doubleForKey("CoordinateLat")
            let oldLon = savedLatLon.doubleForKey("CoordinateLon")
            latitude = oldLat
            longitude = oldLon
            getWeather(oldLat, lon: oldLon)
            let oldCity = savedLocations.valueForKey("LocationCity") as String
            let oldCountry = savedLocations.valueForKey("LocationCountry") as String
            self.locationView.text = "\(oldCity), \(oldCountry)"
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.location.stopUpdatingLocation()
    }
    
    //New location detected, get the coordinates of it.
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        getCity(manager)
        
        if let loc = locations.last as? CLLocation {
            let latLon = loc.coordinate
            latitude = latLon.latitude
            longitude = latLon.longitude
            getWeather(latitude!, lon: longitude!)
        }
    }
    
    //Get the city of the new location.
    func getCity(manager: CLLocationManager) {
        CLGeocoder().reverseGeocodeLocation(manager.location, completionHandler: {(placemarks, error) -> Void in
            
            if (error != nil) {
                println(error.localizedDescription)
                return
            }
            
            if (placemarks.count > 0) {
                let pm = placemarks[0] as CLPlacemark
                self.displayLocationInfo(pm)
            } else {
                println("Problem with geocoder data")
            }
        })
    }
    
    //Pass the data into the custom weather class, get the data back into a closure and add it to global variables.
    func getWeather(lat:Double, lon:Double) {
        var weatherClass = weather()
        weatherClass.getWeather(lat, lon: lon, completionHandler: {(dates:[String], weather:[String], images:[String]) -> Void in
            dispatch_async(dispatch_get_main_queue(), {
                self.datesView = dates
                self.weatherView = weather
                self.imagesView = images
                self.appsTableView?.reloadData()
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            })
        })
    }
    
    //Add the city and country to the locations label.
    func displayLocationInfo(placemark: CLPlacemark) {
        self.locationView.text = "\(placemark.subAdministrativeArea), \(placemark.country)"
    }
    
    //Error getting users current location.
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        println(error.localizedDescription)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //Perform a segue to the map view, passing in the current location's coordinates.
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
    if segue.identifier == "ToMap" {
        let destination = segue.destinationViewController as MapViewController
        destination.currentLat = latitude
        destination.currentLon = longitude
        }
    }
    
    //Perform an unwind segue from the map view, retrieving the new location information.
    @IBAction func unwindToMainView(segue: UIStoryboardSegue) {
        let source = segue.sourceViewController as MapViewController
        let newLat = source.newLat
        let newLon = source.newLon
        latitude = newLat
        longitude = newLon
        let newPlacemark = source.newPlacemark
        displayLocationInfo(newPlacemark!)
        getWeather(newLat!, lon: newLon!)
    }
}