//
//  WeatherCell.swift
//  WeatherForecast
//
//  Created by Michael Tugby on 25/11/2014.
//  Copyright (c) 2014 Michael Tugby. All rights reserved.
//

import UIKit

class WeatherCell: UITableViewCell {
    
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var weatherDesc: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
