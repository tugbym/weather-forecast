//
//  MapViewController.swift
//  WeatherForecast
//
//  Created by Michael Tugby on 03/12/2014.
//  Copyright (c) 2014 Michael Tugby. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate, UIGestureRecognizerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    var currentLat:Double?
    var currentLon:Double?
    
    var newLat:Double?
    var newLon:Double?
    var newPlacemark:CLPlacemark?
    
    @IBOutlet weak var helperText: UITextView!
    
    @IBOutlet var longPress: UILongPressGestureRecognizer!
    
    //When gesture recognizer first detects a long press, place a pin on the current location.
    @IBAction func longPressAction(sender: UILongPressGestureRecognizer) {
        if sender.state == UIGestureRecognizerState.Began {
            let tapPoint: CGPoint = longPress.locationInView(mapView)
            let tapCoordinate: CLLocationCoordinate2D = mapView.convertPoint(tapPoint, toCoordinateFromView: mapView)
            addNewPin(tapCoordinate)
            helperText.text = "Press on your new pin to set it as your new location."
        }
    }
    
    //Custom Pin Annotation, with a set and delete button.
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        if annotation is PinAnnotation {
            let pinAnnotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "myPin")
            
            pinAnnotationView.pinColor = .Purple
            pinAnnotationView.draggable = true
            pinAnnotationView.canShowCallout = true
            pinAnnotationView.animatesDrop = true
            
            let deleteButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
            deleteButton.frame.size.width = 44
            deleteButton.frame.size.height = 44
            deleteButton.backgroundColor = UIColor.redColor()
            deleteButton.setImage(UIImage(named: "trash"), forState: .Normal)
            
            let setButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
            setButton.frame.size.width = 44
            setButton.frame.size.height = 44
            setButton.backgroundColor = UIColor.blueColor()
            setButton.setTitle("Set", forState: UIControlState.Normal)
            
            pinAnnotationView.leftCalloutAccessoryView = deleteButton
            pinAnnotationView.rightCalloutAccessoryView = setButton
            
            return pinAnnotationView
        }
        
        return nil
    }
    
    func mapView(mapView: MKMapView!, annotationView view: MKAnnotationView!, calloutAccessoryControlTapped control: UIControl!)
    {
        //The left accessory (Delete) was pressed.
        if control == view.leftCalloutAccessoryView {
            if let annotation = view.annotation as? PinAnnotation {
                self.mapView.removeAnnotation(annotation)
            }
        }
        //The right accessory (Set) was pressed.
        else {
            if let annotation = view.annotation as? PinAnnotation {
                
                //Get the coordinate of the pin annotation.
                let coord = annotation.coordinate
                newLat = coord.latitude
                newLon = coord.longitude
                
                //Get the current location of the coordinate.
                let location:CLLocation = CLLocation(latitude: newLat!, longitude: newLon!)
                CLGeocoder().reverseGeocodeLocation(location, completionHandler: {(placemarks, error) -> Void in
                    
                    //Error getting the location.
                    if (error != nil) {
                        println(error.localizedDescription)
                        return
                    }
                    
                    //Found a location.
                    if (placemarks.count > 0) {
                        self.newPlacemark = placemarks[0] as? CLPlacemark
                        savedLocations.setValue(self.newPlacemark?.subAdministrativeArea, forKey: "LocationCity")
                        savedLocations.setValue(self.newPlacemark?.country, forKey: "LocationCountry")
                        savedLocations.synchronize()
                        savedLatLon.setDouble(self.newLat!, forKey: "CoordinateLat")
                        savedLatLon.setDouble(self.newLon!, forKey: "CoordinateLon")
                        savedLatLon.synchronize()
                        
                        //Perform an unwind segue to the main view controller.
                        self.performSegueWithIdentifier("returnToMainView", sender: self)
                        
                    //Error with geocoder data.
                    } else {
                        println("Problem with geocoder data")
                    }
                })
            }
        }
    }
    
    //Add delegates.
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self
        longPress.delegate = self
        addCurrentLocal()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //Get the current location's coordinates, create a pin and add it to the map.
    func addCurrentLocal() {
        let location = CLLocationCoordinate2D(latitude: currentLat!, longitude: currentLon!)
        let annotation = PinAnnotation()
        annotation.setCoordinate(location)
        annotation.title = "Your current location"
        mapView.addAnnotation(annotation)
        
        //Center the map on the pin.
        let region = MKCoordinateRegion(center: location, span: MKCoordinateSpan(latitudeDelta: 10, longitudeDelta: 10))
        mapView.setRegion(region, animated: true)
    }
    
    //Create a new pin from the passed in coordinates, and add it to the map.
    func addNewPin(latLon:CLLocationCoordinate2D) {
        let annotation = PinAnnotation()
        annotation.setCoordinate(latLon)
        annotation.title = "Your new location"
        mapView.addAnnotation(annotation)
    }
}
