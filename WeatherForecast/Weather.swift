//
//  Weather.swift
//  WeatherForecast
//
//  Created by Michael Tugby on 26/11/2014.
//  Copyright (c) 2014 Michael Tugby. All rights reserved.
//

import Foundation

class weather {
    func getWeather(lat:Double, lon:Double, completionHandler:(dates:[String], weather:[String], images:[String]) -> Void) {
        
        //Get the URL
        let url = NSURL(string: "http://api.openweathermap.org/data/2.5/forecast/daily?lat=\(lat)&lon=\(lon)&units=metric&mode=json&cnt=7")
        println(url)
        let tempUrl:NSURL = url
        let urlReq = NSURLRequest(URL: tempUrl)
        let queue = NSOperationQueue()
        
        //Start a new asynchronous request to the API.
        NSURLConnection.sendAsynchronousRequest(urlReq, queue: queue, completionHandler: { (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
            
            //API Error.
            if (error != nil) {
                println("API error: \(error), \(error.userInfo)")
            }
            
            //Convert JSON data into a Dictionary.
            var jsonError:NSError?
            var json:NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSDictionary
            
            //JSON error handling.
            if (jsonError != nil) {
                println("Error parsing json: \(jsonError)")
            }
                
            //No issues so far:
            else {
                var weather: [String] = []
                var dates: [String] = []
                var images: [String] = []
                
                //Create an array from the list key in the dictionary.
                var results: NSArray = json["list"] as NSArray
                
                //Iterate through the array and retrieve a timestamp for each day listed.
                if let mydays: Array<AnyObject> = json["list"] as? Array<AnyObject> {
                    for myday in mydays {
                        
                        //Convert each timestamp into a readable date.
                        if let timestamp: Int = myday["dt"] as? Int {
                            var formatter:NSDateFormatter = NSDateFormatter()
                            formatter.dateFormat = "dd/MM/yyyy"
                            let interval:NSTimeInterval = Double(timestamp)
                            let date:NSDate = NSDate(timeIntervalSince1970: interval)
                            let dateString = formatter.stringFromDate(date)
                            
                            //Append each date to an array.
                            dates.append(dateString)
                        }
                    }
                }
                
                //Iterate through the array and retrieve weather data for each day.
                if let mydays: Array<AnyObject> = json["list"] as? Array<AnyObject> {
                    for myday in mydays {
                        if let myweathers: Array<AnyObject> = myday["weather"] as? Array<AnyObject> {
                            for myweather in myweathers {
                                
                                //Append the weather description to an array.
                                if let description: String = myweather["description"] as? String {
                                    weather.append(description)
                                }
                                
                                //Append the weather image name to an array.
                                if let icon: String = myweather["icon"] as? String {
                                    images.append(icon)
                                }
                            }
                        }
                    }
                }
                //Pass the arrays through to a completion handler to access them in a closure later on.
                completionHandler(dates: dates, weather: weather, images: images)
            }
        })
    }
}

